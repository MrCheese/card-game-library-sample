# README #

### What is this repository for? ###

This repository is to illustrate using [Card Game Library](https://bitbucket.org/MrCheese/card-game-library)'s Card and Deck classes. The graphics in this project were taken from [opengameart.org](http://opengameart.org/content/playing-cards-0).

### How do I get set up? ###

This repository holds a Visual Studio 2013 Windows Form C# project. The project depends on a second project, [Card Game Library](https://bitbucket.org/MrCheese/card-game-library), which must also be downloaded in order to compile. .NET 4.5 or later is also required.

Andrew Cheesman