﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardGameLibrary;
using System.Drawing;

namespace CardGameLibrarySample
{
    class PlayingCard : Card
    {
        String _name;
        public String Name
        { get { return _name;  } }
       
        Bitmap _image;
        public Bitmap Image
        { get { return _image; } }

        public PlayingCard(String name, Bitmap image)
        {
            _name = name;
            _image = image;
        }

        public override String ToString()
        {
            return _name;
        }
    }
}
