﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CardGameLibrary;

namespace CardGameLibrarySample
{
    public partial class frmCardShuffler : Form
    {
        PictureBox[] _cardPictures;
        Deck<PlayingCard> _deck = new Deck<PlayingCard>();

        // Innitialise the 5 cards
        PlayingCard[] _cards = { 
                new PlayingCard("Ace of Clubs", global::CardGameLibrarySample.Properties.Resources.c01), 
                new PlayingCard("Ace of Diamonds", global::CardGameLibrarySample.Properties.Resources.d01), 
                new PlayingCard("Ace of Hearts", global::CardGameLibrarySample.Properties.Resources.h01), 
                new PlayingCard("Ace of Spades", global::CardGameLibrarySample.Properties.Resources.s01), 
                new PlayingCard("Five of Spades", global::CardGameLibrarySample.Properties.Resources.s05)
            };

        public frmCardShuffler()
        {
            InitializeComponent();
            initializeDeck();
            initializePictureBoxes();          
        }

        // Setup the deck
        void initializeDeck()
        {
            if (_deck == null) _deck = new Deck<PlayingCard>();
            _deck.Clear();
            for (int i = 0; i < 5; ++i)
                _deck.Add(_cards[i]);
        }

        void initializePictureBoxes()
        {
            int i = 0;
            _cardPictures = new PictureBox[_cards.Length];
            // Connect PictureBoxes to Array.
            _cardPictures[i++] = pictureBox1;
            _cardPictures[i++] = pictureBox2;
            _cardPictures[i++] = pictureBox3;
            _cardPictures[i++] = pictureBox4;
            _cardPictures[i++] = pictureBox5;

            for (i = 0; i < _cardPictures.Length; ++i)
                _cardPictures[i].Visible = true;

            drawCards();
        }

        // Puts the card graphics into the pictureboxes.
        public void drawCards()
        {
            int i = 0;
            foreach (PlayingCard c in _deck)
                _cardPictures[i++].BackgroundImage = c.Image;
        }

        // Shuffle the deck.
        private void button1_Click(object sender, EventArgs e)
        {
            _deck.Shuffle();
            drawCards();
        }

        //Draw a card from the deck
        private void button2_Click(object sender, EventArgs e)
        {
            PlayingCard drawnCard = _deck.Draw();
            if (drawnCard == null) // An empty Deck will return null when Draw is called.
            {
                MessageBox.Show("No more cards left to draw, rebuilding deck.");
                initializeDeck();
                initializePictureBoxes();
                drawCards();
                return;
            }
            MessageBox.Show(drawnCard.ToString() + " drawn."); // Tell the user which card was drawn.
            _cardPictures[_deck.Count].Visible = false; // Hide the last picturebox.
        }
    }
}
